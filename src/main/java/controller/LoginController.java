package controller;

import com.example.map226tomandjerrygui.MainApplication;
import domain.User;
import domain.validator.FriendshipValidator;
import domain.validator.MessageValidator;
import domain.validator.UserValidator;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import repository.database.FriendshipDbRepo;
import repository.database.MessageDbRepo;
import repository.database.UserDbRepo;
import service.Service;
import javafx.fxml.FXML;

import java.io.IOException;
import java.net.URL;

public class LoginController extends ScreenController{

    @FXML
    TextField userField;
    @FXML
    TextField passField;

    @FXML
    protected void loginClick() throws IOException {
        try {

            String[] nameSplit = userField.getText().split("\\s+");
            User tryUser = new User(nameSplit[0], nameSplit[1], passField.getText());

            if (ScreenController.service.loginUser(tryUser))
                startHomePage(tryUser);
            else
                giveWarning("Invalid name/password.");
        }
        catch (ArrayIndexOutOfBoundsException ex){
            giveWarning("Please enter your full name!");
        }
    }

    @FXML void signUpClick() throws IOException {
        registerScreenSwitch();
    }


    @FXML
    protected void startHomePage(User tryUser) throws IOException {

        User currentUser = service.findUserByName(tryUser.getFirstName(), tryUser.getLastName());

        ScreenController.currentUser = currentUser;

        if (ScreenController.service.checkIfAdmin(currentUser))
            adminScreenSwitch();
        else
            homeScreenSwitch();
    }

    public void nameKeyPressed(KeyEvent keyEvent) throws IOException {
        if(keyEvent.getCode().toString().equals("ENTER")) {
            loginClick();
        }
    }

    public void passwordKeyPressed(KeyEvent keyEvent) throws IOException {
        if(keyEvent.getCode().toString().equals("ENTER")) {
            loginClick();
        }
    }
}
