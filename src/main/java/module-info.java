module com.example.map226tomandjerrygui {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires java.sql;

    opens com.example.map226tomandjerrygui to javafx.fxml;
    exports com.example.map226tomandjerrygui;
    exports domain;
    exports service;
    exports repository.database;
    opens controller to javafx.fxml;
    exports controller;

    requires org.apache.pdfbox;

}